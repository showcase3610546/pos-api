import { DateTime } from 'luxon'
import { BaseModel, column, beforeCreate, hasMany, belongsTo } from '@adonisjs/lucid/orm'
import OrderItem from './order_item.js'
import type { HasMany, BelongsTo } from '@adonisjs/lucid/types/relations'
import User from './user.js'

export default class Order extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare orderNo: string

  @column()
  declare totalPrice: number

  @column()
  declare subTotalPrice: number

  @column()
  declare itemCount: number

  @column()
  declare userId: number

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @beforeCreate()
  static async generateOrderNo(order: Order) {
    order.orderNo = new Date().valueOf().toString();
  }

  @hasMany(() => OrderItem)
  declare orderItems: HasMany<typeof OrderItem>

  @belongsTo(() => User)
  declare user: BelongsTo<typeof User>

}