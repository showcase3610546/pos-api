import axios from 'axios'
import env from '#start/env'

export default class ProductsController {


    async index() {
        // const requestQuery = request.qs();

        // let orders;
        const products = await axios.get(env.get('WAREHOUSE_API') + '/api/external/products');

        return {
            message: "success",
            products: products.data.products
        }
    }
}