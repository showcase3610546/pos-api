import Order from '#models/order';
import type { HttpContext } from '@adonisjs/core/http'

export default class OrdersController {

    /**
     * Display a list of resource
     */
    async index({ request }: HttpContext) {
        const requestQuery = request.qs();

        let orders;
        if (requestQuery.limit) {
            orders = await Order.query().orderBy('created_at', 'desc').preload('user').paginate(requestQuery.page, requestQuery.limit);
        } else {
            orders = {
                data: await Order.query().orderBy('created_at', 'desc').preload('user')
            }
        }

        return {
            message: "success",
            orders
        }
    }

    async store({ request }: HttpContext) {
        const requestBody = request.body();

        const order = await Order.create({
            totalPrice: requestBody.totalPrice,
            subTotalPrice: requestBody.subTotalPrice,
            itemCount: requestBody.orderItems.length,
            userId: requestBody.userId
        });

        await order.related('orderItems').createMany(requestBody.orderItems);

        return {
            message: "success",
            order
        }
    }

    async show({ params }: HttpContext) {

        const order = await Order.query().preload('orderItems').preload('user').where('id', params.id).first();
        return {
            message: "success",
            order
        }
    }
}