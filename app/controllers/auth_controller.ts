import { HttpContext } from "@adonisjs/core/http";
import User from '#models/user';
// import { loginValidator } from "#validators/auth";

export default class AuthsController {
    async login(ctx: HttpContext) {
        const request = ctx.request.body();

        //validation
        // await loginValidator.validate(request);

        const user = await User.verifyCredentials(request.email, request.password);
        const token = await User.accessTokens.create(user);

        return {
            message: "Login successful",
            user,
            token: token.value!.release()
        }
    }
}