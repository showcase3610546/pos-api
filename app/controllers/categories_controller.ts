import axios from 'axios'
import env from '#start/env'

export default class CategoriesController {
    async index() {
        // const requestQuery = request.qs();

        // let orders;
        const categories = await axios.get(env.get('WAREHOUSE_API') + '/api/external/categories');

        return {
            message: "success",
            categories: categories.data.categories
        }
    }
}