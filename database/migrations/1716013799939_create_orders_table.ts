import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'orders'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('order_no');
      table.float('total_price').defaultTo(0);
      table.float('sub_total_price').defaultTo(0);
      table.integer('item_count').defaultTo(0);
      table.integer('user_id').unsigned();
      table.foreign('user_id').references('users.id')
      table.timestamp('created_at')
      table.timestamp('updated_at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}