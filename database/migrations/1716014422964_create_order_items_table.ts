import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'order_items'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('order_id').unsigned()
      table.foreign('order_id').references('orders.id')
      table.string('name');
      table.bigInteger('product_id');
      table.integer('qty').defaultTo(0);
      table.float('sub_price').defaultTo(0);
      table.float('total_price').defaultTo(0);
      table.timestamp('created_at')
      table.timestamp('updated_at')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}