/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

import OrdersController from '#controllers/orders_controller';
import router from '@adonisjs/core/services/router'
import { middleware } from './kernel.js';
import AuthsController from '#controllers/auth_controller';
import ProductsController from '#controllers/products_controller';
import CategoriesController from '#controllers/categories_controller';

router.get('/', async () => {
  return {
    hello: 'world',
  }
})


router.group(() => { //api group

  router.post('/login', [AuthsController, 'login']);

  router.group(() => {
    router.post('/orders', [OrdersController, 'store']);
    router.get('/orders', [OrdersController, 'index']);
    router.get('/orders/:id', [OrdersController, 'show']);
    router.get('/products', [ProductsController, 'index']);
    router.get('/categories', [CategoriesController, 'index']);

  }).use(middleware.auth())

}).prefix('api');


